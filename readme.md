# git cheat sheet

Read your group instruction in the text files 

[group1.md](group1.md)

[group2.md](group2.md)

[group3.md](group3.md)

[group4.md](group4.md)

and place your solution in your corresponding text file. Later, we will integrate all solutions into this file.

## Creating repositories

    $ git init                     Erzeugt ein neues Git-Repository
    $ git clone https://...        Kopiert ein komplettes Git-Repository
   
## Staging and committing

    $ git add test.txt             Setzt test.txt in die Stage
    $ git reset HEAD test.txt      Entfernt test.txt aus der Stage, der Inhalt bleibt erhalten
    $ git add .                    Neue und geänderte Dateien werden in die Stage übernommen, gelöschte nicht
    $ git add -u                   Alle Geänderte Dateien werden in die Stage übernommen, Gelöschte geprüft, Neue nicht berücksichtigt
    $ git add -A                   Alle Änderungen werden in die Stage übernommen
    $ git diff --staged            Zeigt Änderungen zwischen letztem committeten Stand an
	$ git commit                   Öffnet den in Git eingestellten Editor zur Eingabe eines Commit-Kommentars, anschließend commit
    $ git commit -a                Kurzform für git add -A und git commit
    $ git commit -m "My message"   Wie git commit, jedoch wird der Commit-Kommentar direkt angegeben
	
## Inspecting the repository and history

    $ git diff										--> Unterschiede der unstaged Dateien über das gesamte Repository anzeigen
    $ git diff --staged								--> Unterschiede der gestageten Dateien über das gesamte Repository anzeigen
    $ git diff test.txt 							--> vergleicht aktuellen Dateinhalt mit dem Stand des letzten Commits 
    $ git diff --theirs								--> nach fetch Änderungen der anderen anzeigen
    $ git diff --ours								--> nach fetch eigene Änderungen anzeigen
    $ git log										--> Anzeige der lokalen Commits 
    $ git log --oneline								--> Anzeige der lokalen Commits (je Commit nur eine Zeile)
    $ git log --oneline --all						--> Anzeige aller Commits (einzeilig)
    $ git log --oneline --all --graph				--> Anzeige aller Commits (einzeilig als Graph)
    $ git log --oneline --all --graph --decorate	--> Anzeige aller Commits (einzeilig als Graph, dekoriert mit *)
    $ git log --follow -p -- filename				--> Anzeige der Commits einer Datei (mit Umbenennung)
    $ git log -S'static void Main'					--> Suche von Commits, die diese Zeichenkette geändert haben
    $ git log --pretty=format:"%h - %an, %ar : %s" 	--> Anzeige der Commits mit individueller Formatierung
		--> Ausgabeformatierung: <Hash (7 Zeichen)> - <Benutzername>, <Timestamp des Commits> : <Kommitkommenar>
		--> Beispiel: 9ebd781 - Benutzername, 17 hours ago : testa created online with Bitbucket

## Managing branches

    $ git checkout master		Setzen des Pointers auf den Master-Branch.
    $ git branch feature1		Branch "feature1" anlegen.
    $ git checkout -b work		Anlegen eines neuen Branches "work" und setzen des Pointers auf diesen neuen Branch.
    $ git checkout work			Wechsel in den Branch "work".
    $ git branch				Anzeigen der lokalen Branches.
    $ git branch -v				Zeigt die lokalen Branches mit ihren letzten Commit-Hashes und -Messages.
    $ git branch -a				Zeigt die lokalen und die entfernten Branches an.
    $ git branch --merged		Zeigt die lokalen Branches, deren Stand in den aktuell ausgecheckten Branch integriert ist.
    $ git branch --no-merged	Zeigt die lokalen Branches, deren Stand in den aktuell ausgecheckten Branch nicht integriert ist.
    $ git branch -d work		Löscht den Branch work, wenn dieser vollständig in seinem Parent-Branch integriert ist.
    $ git branch -D work		Löscht den Branch work, auch wenn er nicht vollständig in seinen Parent-Branch integriert sein sollte.

## Merging

Exercise:

Explain the following commands with one sentence:

    $ git checkout work	In den work Branch wechseln
	
    $ git checkout master	In den master Branch wechseln
	
    $ git merge work	Merge von work in master Branch
	
    $ git merge --abort	Den Merge abbrechen
	
    $ git cat-file -p a3798b	Zeigen aller rel. Informationen zum Hash a3798b...

Example:

    $ git status         Shows the current state of the repository


Here are some standard situations, and a git command. Draw the result, and add some explanation:

Example 1:

    A - B (master)
         \
          C (work)
    
    $ git checkout master
    $ git merge work

Result and explanation here:

    A - B - C (master, work)
     

Nach checkout master, zeigt der HEAD auf master.
Fast Forward Merge. Master und Work zeigen auf den gleichen Hash.

Example 2:

    A - B - C - D (master)
         \
          X - Y (work)
    
    $ git checkout master
    $ git merge work



Result and explanation here:

    A - B - C - D - E(master)
         \     	 /
		   X - Y (work) 
		   
Wie in Ex. 1.
Echter Merge von work in den master, dh. neuer Commit notwendig. Konflikt könnte auftreten.



Example 3:

    A - B - C - D (master)
         \
          X - Y (work)
    
    $ git checkout work
    $ git merge master
    $ git checkout master
    $ git merge work

Result and explanation here:

      A - B - C -D
         \       \
          X - Y - Z (master, work)

Wechseln in work Branch.
Aenderungen vom master Branch in work mergen, Commit Z ist erzeugt.
Wechseln in master.
Fast Forward.


First, add your answers in this file and commit/push.
Later, integrate your answers into the README.md.